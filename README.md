# Docker Swarm Module for Ansible

This is the Docker Swarm module for Ansible.  It can be used to set up the Docker Swarm manager and nodes.

## Requirements

- ansible 
- docker-py >= 2.2.1


